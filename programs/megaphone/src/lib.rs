use anchor_lang::prelude::*;
use anchor_lang::solana_program::system_program;

declare_id!("CrdH2x2S8pbmpyVmdDpSSvEJ81LyiZZEaRjJcTKT1H4Q");

#[program]
pub mod megaphone {
    use super::*;
    pub fn initialize(ctx: Context<Initialize>, audience_account_bump: u8, audience_name: String ) -> ProgramResult {
        ctx.accounts.audience_account.bump = audience_account_bump;
        ctx.accounts.audience_account.name = audience_name;
        ctx.accounts.audience_account.authority = *ctx.accounts.authority.to_account_info().key;
        Ok(())
    }
    pub fn register(ctx: Context<Register>, checkin_account_bump: u8, token: String) -> ProgramResult {
        ctx.accounts.checkin_account.bump = checkin_account_bump;
        ctx.accounts.checkin_account.token = token;
        ctx.accounts.checkin_account.authority = *ctx.accounts.authority.to_account_info().key;
        ctx.accounts.checkin_account.audience = *ctx.accounts.audience_account.to_account_info().key;
        ctx.accounts.audience_account.register_count += 1;

        Ok(())
    }
}

#[derive(Accounts)]
#[instruction(audience_account_bump: u8)]
pub struct Initialize<'info> {
    #[account(
        init,
        seeds = [
            b"Audience".as_ref(),
            authority.key().as_ref()
        ],
        bump = audience_account_bump,
        payer = authority,
        space = 300,
    )]
    pub audience_account: Account<'info, Audience>,
    #[account(mut)]
    pub authority: Signer<'info>,
    #[account(address = system_program::ID)]
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(checkin_account_bump: u8, token: String)]
pub struct Register<'info> {
    #[account(mut)]
    pub audience_account: Account<'info, Audience>,

    #[account(
        init,
        seeds = [
            b"register".as_ref(),
            authority.key().as_ref(),
        ],
        bump = checkin_account_bump,
        payer = authority,
        space = 300,
    )]
    pub checkin_account: Account<'info, Checkin>,

    #[account(mut)]
    pub authority: Signer<'info>,

    #[account(address = system_program::ID)]
    pub system_program: Program<'info, System>,
}


#[account]
#[derive(Default)]
pub struct Audience {
    pub bump: u8,
    pub name: String,
    pub authority: Pubkey,
    pub register_count: u8
}

#[account]
#[derive(Default)]
pub struct Checkin {
    pub authority: Pubkey,
    pub bump: u8,
    pub token: String,
    pub audience : Pubkey
}



