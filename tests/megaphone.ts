import * as anchor from '@project-serum/anchor';
import { Program } from '@project-serum/anchor';
import { Megaphone } from '../target/types/megaphone';

import assert from "assert";
 const NFT  = 'H9bczEhpJJu4Y8LjsEWdYQuxZydv7FSRLUkqmHAbcnAZ'
 const NFT2 = 'H9bczEhpJJu4Y8LjsEWdYQuxZydv7FSRLUkqmHAbcnAZ'

describe('megaphone', async () => {

  // Configure the client to use the local cluster.
  const provider = anchor.Provider.env();
  anchor.setProvider(provider);

  const token = anchor.utils.sha256.hash(NFT+'pa$$word'+provider.wallet.publicKey)
  const token2 = anchor.utils.sha256.hash(NFT2+'pa$$word2'+provider.wallet.publicKey)

  const program = anchor.workspace.Megaphone as Program<Megaphone>;

 
  const [audienceAccount, audienceAccountBump] = 
  await anchor.web3.PublicKey.findProgramAddress(
        [Buffer.from("influenceur")],
        program.programId
      );

  const [checkinAccount, checkinAccountBump] =
  await anchor.web3.PublicKey.findProgramAddress(
    [
    Buffer.from("register"),
    audienceAccount.toBuffer(),
    new anchor.BN(0).toArrayLike(Buffer),
    ],
    program.programId
  );
  const [checkinAccount2, checkinAccountBump2] =
  await anchor.web3.PublicKey.findProgramAddress(
    [
      Buffer.from("register"),
      audienceAccount.toBuffer(),
      new anchor.BN(1).toArrayLike(Buffer),
      ],
      program.programId
  );

  it("Initializes audience with his name", async () => {
    await program.rpc.initialize(new anchor.BN(audienceAccountBump), 
    'lexa Moon crypto', {
      accounts: {
        audienceAccount: audienceAccount,
        authority: provider.wallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      },
    }
    );

    let audience = await program.account.audience.fetch(
      audienceAccount
    );
    assert.equal('lexa Moon crypto', audience.name);
    assert.equal(provider.wallet.publicKey.toBase58(),audience.authority)
  });

  it("one can get registred in the audience", async () => {
    await program.rpc.register(new anchor.BN(checkinAccountBump), 
    token, {
      accounts: {
        audienceAccount: audienceAccount,
        checkinAccount: checkinAccount,
        authority: provider.wallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      },
    }
    );

    let checkin = await program.account.checkin.fetch(
      checkinAccount
    );
    assert.equal(token, checkin.token);
  });

  it("another one can get registred in the audience", async () => {

    // const otherUser = anchor.web3.Keypair.generate();
    const signature = await program.provider.connection.requestAirdrop(provider.wallet.publicKey, 1000000000);
    const signature2 = await program.provider.connection.requestAirdrop(provider.wallet.publicKey, 1000000000);
    await program.provider.connection.confirmTransaction(signature);
    await program.provider.connection.confirmTransaction(signature2);

    await program.rpc.register(new anchor.BN(checkinAccountBump2), 
    token2, {
      accounts: {
        audienceAccount: audienceAccount,
        checkinAccount: checkinAccount2,
        authority: provider.wallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      },
    }
    );

    let checkin = await program.account.checkin.fetch(
      checkinAccount2
    );
    assert.equal(token2, checkin.token);
  });

  it("grant access to audience with NFT", async () => {

    let checkin_list = await program.account.checkin.all();
    for (const checkin in checkin_list) {
      if (Object.prototype.hasOwnProperty.call(checkin_list, checkin)) {
        const element = checkin_list[checkin];
        if (element.account.token == token2) {
          assert.ok('grant acess to audience with NFT ')
        }
      }
    }

  });
  
});
