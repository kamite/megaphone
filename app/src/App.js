import React, { useCallback, useEffect, useState } from "react";
import {
  ConnectionProvider,
  WalletProvider,
} from "@solana/wallet-adapter-react";
import { WalletDialogProvider } from "@solana/wallet-adapter-material-ui";
import { SnackbarProvider, useSnackbar } from "notistack";
import { createTheme, ThemeProvider } from "@material-ui/core";
import { blue, orange } from "@material-ui/core/colors";
import Main from "./components/Main";
import { programID, network, wallets, preflightCommitment } from "./utils/config";
import { useWallet } from "@solana/wallet-adapter-react";
import { Connection } from "@solana/web3.js";
import { Program, Provider, web3, BN } from "@project-serum/anchor";
import idl from "./idl.json";


const theme = createTheme({
  palette: {
    primary: {
      main: blue[300],
    },
    success: {
      main: orange[300],
    },
  },
  overrides: {
    MuiButtonBase: {
      root: {
        justifyContent: "flex-start",
      },
    },
    MuiButton: {
      root: {
        textTransform: undefined,
        padding: "12px 16px",
        fontWeight: 600,
      },
      startIcon: {
        marginRight: 8,
      },
      endIcon: {
        marginLeft: 8,
      },
      label: {
        color: "white",
      },
    },
    MuiLink: {
      root: {
        color: "initial",
      },
    },
  },
});

// Nest app within <SnackbarProvider /> so that we can set up Snackbar notifications on Wallet errors
function AppWrappedWithProviders() {
  const { enqueueSnackbar } = useSnackbar();
  const wallet = useWallet();
  console.log(wallet)

  const [registers, setRegisters] = useState("");
  const [audiences, setAudiences] = useState("");

  useEffect(() => {
    // Call Solana program for audience account
    async function getRegisters() {
      const connection = new Connection(network, preflightCommitment);
      const provider = new Provider(connection, wallet, preflightCommitment);
      const program = new Program(idl, programID, provider);
      try {
        const checkins = await program.account.checkin.all();
        if(checkins){
          setRegisters(checkins);
        }
      } catch (error) {
        console.log("could not getRegisters: ", error);
      }
    }
    getRegisters();
    // Call Solana program for audience account
    async function getAudiences() {
      const connection = new Connection(network, preflightCommitment);
      const provider = new Provider(connection, wallet, preflightCommitment);
      const program = new Program(idl, programID, provider);
      try {
        const audiences = await program.account.audience.all();
        if(audiences){
          setAudiences(audiences);
        }
      } catch (error) {
        console.log("could not audiences: ", error);
      }
    }
    getAudiences();
    
  }, []);

  const onWalletError = useCallback(
    (error) => {
      enqueueSnackbar(
        error.message ? `${error.name}: ${error.message}` : error.name,
        { variant: "error" }
      );
      console.error(error);
    },
    [enqueueSnackbar]
  );

  // Wrap <Main /> within <WalletProvider /> so that we can access useWallet hook within Main
  return (
    <WalletProvider wallets={wallets} onError={onWalletError} autoConnect>
      <WalletDialogProvider>
        <Main
          network={network}
          registers={registers}
          audiences={audiences}
        />
      </WalletDialogProvider>
    </WalletProvider>
  );
}

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider>
        <ConnectionProvider endpoint={network}>
          <AppWrappedWithProviders />
        </ConnectionProvider>
      </SnackbarProvider>
    </ThemeProvider>
  );
}
