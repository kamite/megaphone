import React, { useEffect, useState } from "react";
import { useWallet } from "@solana/wallet-adapter-react";
import { Connection, clusterApiUrl, PublicKey } from "@solana/web3.js";
import { Program, Provider, web3, BN } from "@project-serum/anchor";
import { Box, Container, Grid, Typography, Card,CardActions, CardContent, CardMedia} from "@material-ui/core";
import { useSnackbar } from "notistack";
import idl from "../idl.json";
import audienceConfig from "../audience/audience.json";
import { preflightCommitment, programID } from "../utils/config";
import Navbar from "./Navbar";
import Footer from "./Footer";
import Intro from "./Intro";
import Register from "./Register"
import { getParsedNftAccountsByOwner} from "../utils/getParsedNftAccountByOwner";
import CreateAudienceButton from "./CreateAudienceButton";

const propTypes = {};

const defaultProps = {};

const NFT = audienceConfig.nft;

export default function Main({network, registers, audiences }) {
  const { enqueueSnackbar } = useSnackbar();
  const wallet = useWallet();
  const audienceAccount = new PublicKey(audienceConfig.audienceAccount)
  const [canRegister, setCanRegister] = useState(false)
  const [registred, setRegistred] = useState(false)
  const [audience, setAudience] = useState({
    name: null,
    count: null,
    authority: null
  });

  const [hasaccess, setHasaccess] = useState({
    "hasaccess": false,
    "tokenAdress" : ""
  });
 
  useEffect(() => {
    // Call Solana program for audience account
    async function getAudience() {

      const connection = new Connection(network, preflightCommitment);
      const provider = new Provider(connection, wallet, preflightCommitment);
      const program = new Program(idl, programID, provider);
      try {
        const account = await program.account.audience.fetch(audienceAccount);
        let NFTS = await getParsedNftAccountsByOwner({publicAddress: wallet.publicKey.toBase58()})
        let canRegister = false
        // check if user can register
        for (const key in NFTS) {
          if (Object.hasOwnProperty.call(NFTS, key)) {
            const nft = NFTS[key];
            if( nft.mint.toBase58() == audienceConfig.nft){
              canRegister = true
            }
          }
        }

        for (const key in registers) {
            console.log(Object.hasOwnProperty.call(registers, key))
          if (Object.hasOwnProperty.call(registers, key)) {
            const checkin = registers[key];
            console.log(checkin)
            if (wallet.publicKey.toBase58() == checkin.account.authority.toBase58()) {
              setRegistred(checkin.account.token)
            }  
          }
        }
        setAudience({
          name: account.name,
          count: account.registerCount,
          authority: account.authority,
        });
        setCanRegister(canRegister)
      } catch (error) {
        console.log("could not getAudience: ", error);
      }
    }

    if (!!audienceAccount) {
      getAudience();
    }
 
  }, [audienceAccount, network, wallet]);

  return (
    <Box height="100%" display="flex" flexDirection="column">
        <Box flex="1 0 auto">
        <Navbar audience={audience} />
        <Container>
          <Grid container spacing={3}>
          <Grid item xs={12}>
          <Grid item xs={12}>
              <Intro
                wallet={wallet}
                audience={audience}
              />
            </Grid>
          </Grid>
          {wallet.connected?(
            <>
          <Grid item xs={6}>
            <Card sx={{ maxWidth: 345 }}>
              <CardMedia
                component="img"
                height="300"
                image="/images/audience_xxl.jpeg"
                alt="audience illustration"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  Créez une audience
                </Typography>
                <Typography variant="body2" color="text.success">
                 Créez une audience pour donner l'accès à votre contenu sur la présentation d'un NFT
                </Typography>
              </CardContent>
              <CardActions>
                <CreateAudienceButton network={network} audiences={audiences} />
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6}>
          <Card sx={{ maxWidth: 345 }}>
              <CardMedia
                component="img"
                height="300"
                image="/images/auditeur.jpeg"
                alt="audience illustration"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  Connectez vous à l'audience {audience.name}
                </Typography>
                <Typography variant="body2" color="text.success">
                  Enregistrez vous ou connectez vous pour avoir accès au contenu de l'audience {audience.name}
                </Typography>
              </CardContent>
              <CardActions>
                {wallet.connected && (wallet.publicKey.toBase58() != audienceConfig.authority)  ?  ( 
                    <Register
                        registred={registred}
                        canRegister={canRegister}
                        audienceConfig={audienceConfig}
                        audience={audience}
                        NFT = {NFT}
                        programID={programID}
                        network={network}
                        audienceAccount={audienceAccount}
                      />): "" }
          
              </CardActions>
            </Card>
            </Grid>
            </>
          ):""}
          </Grid>
        </Container>
        </Box>
      <Footer programID={programID} audienceAccount={audienceAccount} />
    </Box>
  );
}

Main.propTypes = propTypes;
Main.defaultProps = defaultProps;
