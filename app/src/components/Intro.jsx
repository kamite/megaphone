import React from "react";
import { Box, Typography} from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/styles";
import Connexion from "./Connexion";
import { WalletMultiButton } from "@solana/wallet-adapter-material-ui";

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(1),
    "&.hidden": {
      visibility: "hidden",
    },
  },
  connected: {
    color: green[500],
  },
  connectedBubble: {
    backgroundColor: green[500],
    height: 12,
    width: 12,
    borderRadius: "50%",
    marginRight: theme.spacing(0.5),
  },
  title: {
    fontWeight: 700,
  },
}));

export default function Intro({
  wallet,
  audience
}) {
  const classes = useStyles();
  return (
    <Box textAlign="center">
      <Typography
          component="h2"
          variant="h2"
          gutterBottom
          className={classes.title}
        >
          AudienceMaker
      </Typography>
      {wallet.connected?(
      <Typography
          variant="h3" gutterBottom component="div"
          className={classes.title}
        >
          Bienvenue sur l'audience {audience.name}
      </Typography>):
      (<Typography>
          Pour commencer, connectez vous au réseau Solana via votre wallet
          <Box gutterBottom>
            
           <WalletMultiButton />
          </Box>
      </Typography>
        )
       }
    </Box>
  );
}
