import React from "react";
import { green } from "@material-ui/core/colors";
import {
  Box,
  makeStyles,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    button: {
      marginTop: theme.spacing(1),
      "&.hidden": {
        visibility: "hidden",
      },
    },
    connected: {
      color: green[500],
    },
    connectedBubble: {
      backgroundColor: green[500],
      height: 12,
      width: 12,
      borderRadius: "50%",
      marginRight: theme.spacing(0.5),
    },
    title: {
      fontWeight: 700,
    },
  }));

export default function Connexion(wallet) {
  const classes = useStyles();
  return (
    <>
    {wallet.connected ? (
        <Box display="flex" alignItems="center" justifyContent="center">
          <Box className={classes.connectedBubble} />
          <Typography variant="body1" className={classes.connected}>
            Connecté!
          </Typography>
        </Box>
      ) : ""}
    </>
  );
}
