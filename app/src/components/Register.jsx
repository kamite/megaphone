import React from "react";
import {useRef, useState} from "react";
import { Box, Button,Typography, Modal, TextField } from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/styles";
import { useWallet } from "@solana/wallet-adapter-react";
import { Connection } from "@solana/web3.js";
import * as anchor from '@project-serum/anchor';
import { preflightCommitment} from "../utils/config";
import idl from "../idl.json";
import { useSnackbar } from "notistack";


const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  border: '1px solid whitesmoke',
  boxShadow: 24,
  p: 4,
};

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(1),
    "&.hidden": {
      visibility: "hidden",
    },
  },
  connected: {
    color: green[500],
  },
  connectedBubble: {
    backgroundColor: green[500],
    height: 12,
    width: 12,
    borderRadius: "50%",
    marginRight: theme.spacing(0.5),
  },
  title: {
    fontWeight: 700,
  },
}));

export default function Register({
  registred,
  canRegister,
  audienceConfig,
  audience,
  NFT,
  programID,
  network,
  audienceAccount
}) {
  const { enqueueSnackbar } = useSnackbar();
  const wallet = useWallet();
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [grantAccess, setGrantAccess] = React.useState(false);
  const [token, setToken] = React.useState('');
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const passwordRef = useRef('')

  //declare useEffect
  const [checkinAccount, setCheckinAccount] = useState({
    account:'',
    accountBump:''
  })
  const [checkin, setCheckin] = useState({
    user:'',
    audience:''
  })

  const getCheckinAccount = async () => {
    let account,accountBump = null;
    [account, accountBump] = await anchor.web3.PublicKey.findProgramAddress(
      [Buffer.from("register"),
      wallet.publicKey.toBuffer()],
      programID
    );
    setCheckinAccount({ account, accountBump });
    return [account, accountBump]
  };

  async function getProvider() {
    const connection = new Connection(network, preflightCommitment);
    const provider = new anchor.Provider(connection, wallet, preflightCommitment);
    return provider;
  }

  const genToken = async () =>{
    let [CheckinAccount, CheckinAccountBump] = await getCheckinAccount()
    console.log(CheckinAccount, CheckinAccountBump)
    const password = passwordRef.current.value
    const provider = await getProvider()
    setToken(anchor.utils.sha256.hash(NFT+password+provider.wallet.publicKey))
    if (registred == anchor.utils.sha256.hash(NFT+password+provider.wallet.publicKey)){
      setGrantAccess(true)
    }else{
      await registerToAudience(CheckinAccount, CheckinAccountBump, anchor.utils.sha256.hash(NFT+password+provider.wallet.publicKey))
    }
    handleClose()
  }

  async function registerToAudience(CheckinAccount, CheckinAccountBump, token) {
    console.log(CheckinAccount, CheckinAccountBump)
    const provider = await getProvider();
    const program = new anchor.Program(idl, programID, provider);
    try {
      await program.rpc.register(new anchor.BN(CheckinAccountBump), 
      token, {
        accounts: {
          audienceAccount: audienceAccount,
          checkinAccount:  CheckinAccount,
          authority: provider.wallet.publicKey,
          systemProgram: anchor.web3.SystemProgram.programId,
        },
      }
    );
      const account = await program.account.checkin.fetch(CheckinAccount);
      setCheckin({
        user: account.authority,
        audience: account.audience
      });
      enqueueSnackbar("Vous êtes enregistré à l'audience : " + audience.name, { variant: "success" });
    } catch (error) {
      console.log("Transaction error: ", error);
      console.log(error.toString());
      enqueueSnackbar(`Error: ${error.toString()}`, { variant: "error" });
    }
  }


  return (
    <Box textAlign="center">
      {!registred? (
        <Typography variant="body1">
        Enregistrez vous à l'audience {audience.name} pour profiter du contenu privé
      </Typography>
      ):""}
      { wallet.connected  && canRegister ? (
      <Box>
          {!grantAccess? (<Button color="secondary" variant="contained" onClick={handleOpen}>
            {registred ? ("Se connecter"):("S'enregister")}
          </Button>): (<Typography>
            ça y est, vous avez accès a tout le contenu
          </Typography>) }
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="parent-modal-title"
            aria-describedby="parent-modal-description"
          >
            <Box sx={{ ...style, }}>
              <h2 id="parent-modal-title">Entrez un mot de passe </h2>
              <Typography>Nous avons besoin d'un mot de passe pour vous identifier</Typography>
              <TextField
                required
                id="outlined-required"
                label="Mot de passe"
                fullWidth={true}
                inputRef={passwordRef}
              />
              <Button color="secondary"
                variant="contained"
                style={{ float:'right', marginTop:'10px'}}
                onClick={genToken}>
              Valider</Button>
            </Box>
          </Modal>
      </Box>):
          <Typography style={{color: "red"}}>
          Vous ne disposez pas du NFT nécessaire dans votre wallet pour vous enregistrer à {audience.name}
        </Typography>
      } 
    </Box>
  );
}
