import {React} from "react";
import { AppBar, Container, makeStyles, Toolbar, Typography } from "@material-ui/core";
import { WalletMultiButton } from "@solana/wallet-adapter-material-ui";
import { useWallet } from "@solana/wallet-adapter-react";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "transparent",
    boxShadow: "none",
  },
  toolbar: {
    justifyContent: "space-between",
  },
}));

export default function Navbar({audience}) {
  const classes = useStyles();
  const wallet = useWallet();

  return (
    <AppBar position="static" className={classes.root}>
      <Container maxWidth="xl">
        <Toolbar className={classes.toolbar}>
        <img src="/images/megaphone.png" alt="PB Vote" height={70} />
          <WalletMultiButton />
        </Toolbar>
      </Container>
    </AppBar>
  );
}
