import React, { useState } from "react";
import { Box, Button, Typography, Modal, TextField } from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/styles";
import { useWallet } from "@solana/wallet-adapter-react";
import { Connection} from "@solana/web3.js";
import { Program, Provider, web3, BN } from "@project-serum/anchor";
import { preflightCommitment, programID } from "../utils/config";
import idl from "../idl.json";
import { useSnackbar } from "notistack";
import audienceConfig from "../audience/audience.json";

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(1),
    "&.hidden": {
      visibility: "hidden",
    },
  },
  connected: {
    color: green[500],
  },
  connectedBubble: {
    backgroundColor: green[500],
    height: 12,
    width: 12,
    borderRadius: "50%",
    marginRight: theme.spacing(0.5),
  },
  title: {
    fontWeight: 700,
  },
}));

export default function CreateAudienceButton({
  network,
  audiences
}) {
  const wallet = useWallet();
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const audienceRef = React.useRef('')
  const { enqueueSnackbar } = useSnackbar();
  const [audienceAccount, setAudienceAccount] = useState()
  const [audience, setAudience] = useState({
    name: null,
    count: null,
    authority: null
  });
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '1px solid whitesmoke',
    boxShadow: 24,
    p: 4,
  };

  const isCurrentAudienceAuthority = () =>{
    return audienceConfig.authority == wallet.publicKey.toBase58()
  }
  const getAudienceAccount = async (wallet) => {
  let account,
    accountBump = null;
  [account, accountBump] = await web3.PublicKey.findProgramAddress(
      [
        Buffer.from("Audience"),
        wallet.publicKey.toBuffer()
      ],
      programID
    );
    console.log(account, accountBump)
    setAudienceAccount({ account: account, accountBump: accountBump });
    return [account, accountBump]
  }

  async function getProvider() {
    const connection = new Connection(network, preflightCommitment);
    const provider = new Provider(connection, wallet, preflightCommitment);
    return provider;
  }

  function getAudienceData(){
    let audienceNames = []
    let audienceAuthorities = []
    for (const key in audiences) {
      if (Object.hasOwnProperty.call(audiences, key)) {
        const audience = audiences[key];
        console.log(audience.publicKey.toBase58())
        audienceNames.push(audience.account.name)
        audienceAuthorities.push(audience.account.authority.toBase58())
      }
    }
    return [audienceNames, audienceAuthorities]
  }

  // Initialize the program if this is the first time its launched
  async function initializeAudience() {
    let audienceName = audienceRef.current.value
    let [accountAudience, accountBumpAudience] = await getAudienceAccount(wallet)
    const provider = await getProvider();
    const program = new Program(idl, programID, provider);
    const [audienceNames, audienceAuthorities] = getAudienceData()
    if ( audienceName.length > 20 ) {
      enqueueSnackbar("Audience name must be shorter than 20 chars", { variant: "error" });
      return false
    }
    if (audienceNames.includes(audienceName)){
      enqueueSnackbar("This audience name is already in use, please kindly find another name", { variant: "error" });
      return false
    }
    if (audienceAuthorities.includes(wallet.publicKey.toBase58())){
      enqueueSnackbar("You already have an audience, use another wallet to create a new one", { variant: "error" });
      handleClose()
      return false
    }
    try {
      console.log(provider.wallet.publicKey.toBase58(), accountAudience.toBase58(), accountBumpAudience  )
      await program.rpc.initialize(new BN(accountBumpAudience),audienceName, {
        accounts: {
          audienceAccount: accountAudience,
          authority: provider.wallet.publicKey,
          systemProgram: web3.SystemProgram.programId,
        },
      });
      const account = await program.account.audience.fetch(accountAudience);
      console.log(account)
      setAudience({
        name: account.name,
        count: account.registerCount,
        authority: account.authority
      });
      enqueueSnackbar("Audience account initialized with" + {audienceName}, { variant: "secondary" });
    } catch (error) {
      console.log("Transaction error: ", error);
      console.log(error.toString());
      enqueueSnackbar(`Error: ${error.toString()}`, { variant: "error" });
    }
    handleClose()
  }

  async function audienceNameExist(audienceName){

  }
  return (
    <Box textAlign="center">
      {wallet.connected && (
          <Box >
            <Button
              color="secondary"
              variant="contained"
              onClick={handleOpen}
              className={classes.button}
              disabled={isCurrentAudienceAuthority}
            >
              Créer une audience
            </Button>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="parent-modal-title"
              aria-describedby="parent-modal-description"
            >
              <Box sx={{ ...style, }}>
                <h2 id="parent-modal-title">Nom de l'audience </h2>
                <Typography>Choisissez le nom de votre audience (max 30 char)</Typography>
                <TextField
                  required
                  id="outlined-required"
                  label="Nom de l'audience"
                  fullWidth={true}
                  inputRef={audienceRef}
                />
                <Button color="secondary"
              variant="contained"
              onClick={initializeAudience}>
                Valider</Button>
              </Box>
          </Modal>
          </Box>
        )}
    </Box>
  );
}
